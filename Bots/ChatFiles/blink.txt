
[IntroStrings]
"I'm back, baby!"
"Back in my old stomping ground."
"And she's back in the game!"

[FragStrings]
"Hahaha!"
"Whee!"
"Whoa!"
"Wicked!"
"Yeah!"
"Just in time."
"Got ya!"
"Gotcha."
"Nailed it!"
"Right on target."
"Bombs away!"
"Here ya go!"
"You need a time out."
"Cracking!"
"Hahaha, last one there's a rotten egg!"
"No one's gonna stop me!"
"Aww yeah!"
"You got what's coming to ya!"
"That felt good!"
"Squished!"
"Sorry, big guy!"
"Who's next?"
"And stay down!"
"You got it!"
"Check me out!"
"She shoots, she scores, hahaha!"
"It's in the bag!"
"Oh, $player_killed! Hahaha, got your favorite!"
"Maybe you'll still get your chance. Who knows what the future holds?"

[KilledStrings]
"Ever get that feeling of deja vu?"
"Let's try that again."
"Now, where were we?"
"Wait for it..."
"Well that just happened."
"Back to work!"
"Aw, rubbish!"
"Aww, no fair!"
"Be right back!"
"Eat my dust!"

[RoamingStrings]
"Let's get to it already!"
"Oi! This is no time for standin' around."
"Look out world! $player_inlead's here."
"Don't forget to stretch!"
"Keep calm and trace her on!"
"The world could always use more heroes!"
"$player_random_notself! I'll race ya!"
"What'cha lookin' at?"

[RareRoamingStrings]
"Cheers, love! The cavalry's here!"
"I have this under control!"
"$player_inlead is coming to town!"

[LosingRoamingStrings]
"Wow. It's an honor to meet $player_inlead!"
"Hey, you're so good at this, $player_inlead!"

[WinStrings]
"All eyes on me!"
"Excelsior!"
"Sensational!"
"Looks like you need a time out!"

[LoseStrings]
"Time's up!"
"Wait for me!"
"You can be my wingman, anytime!"
"$player_inlead, you're the real hero!"
"$player_inlead, you were such an inspiration to me!"

[FrustratedStrings] // Spammer!
"Who's ready for some fireworks?"

[EnragedStrings] // You fail it!
"And she's back in the game!"

[DemoralizedStrings] // Your skill is not enough!
"Aw, rubbish match!"

[PissedStrings] // Llama!
"Aw, rubbish, mate!"

